import React, { Component } from 'react';
import Wrapper from "../../hoc/Wrapper";
import Modal from "../../components/UI/Modal/Modal";

class SimpleBuilder extends Component {
  render() {
    return (
      <div className="SimpleBuilder">
        <Wrapper>
          <Modal/>
        </Wrapper>
      </div>
    );
  }
}

export default SimpleBuilder;
