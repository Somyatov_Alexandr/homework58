import React from 'react';
import ReactDOM from 'react-dom';
import SimpleBuilder from './containers/SimpleBuilder/SimpleBuilder';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<SimpleBuilder />, document.getElementById('root'));
registerServiceWorker();
