import React from 'react';
import Wrapper from "../../../hoc/Wrapper";

const Modal = props => (
    <Wrapper>
      {/*<Backdrop show={props.show} clicked={props.closed}/>*/}

      <div className="modal show" style={{
        // transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        // opacity: props.show ? '1' : '0'
        display: 'block'
      }}>
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{props.title}</h5>
              <button type="button" className="close" onClick={props.closed}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {props.children}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary">Save changes</button>
              <button type="button" className="btn btn-secondary" onClick={props.closed}>Close</button>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
);

export default Modal;